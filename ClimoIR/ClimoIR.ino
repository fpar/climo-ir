//Parametros de Calibración - Copie y pegue este rango ------------------------------------
#define N 3
#define debug false
//define types of device
#define FUJITSU 1
#define KENDAL 2
#define DAITSU 3
#define ALLOWDISABLE false

int device = DAITSU;
String OFF = "";
int periodo = 0;
int decodedchar = 65;
int decodedchar2 = 65;


// --- Base for using DHT22 in Arduino ---

#include "IRremote.h"
#include "dht.h"

IRsend irsend;
dht DHT;

int temp = 0;
int humi = 0;
int vamp = 1;
long to = millis();
long tf = millis();
long dt = 0;
float pote = 5;
//int chk = 0;

struct
{
  uint32_t total;
  uint32_t ok;
  uint32_t crc_error;
  uint32_t time_out;
  uint32_t connect;
  uint32_t ack_1;
  uint32_t ack_h;
  uint32_t unknown;
}stat = {0,0,0,0,0,0,0,0};


// ---- End of Base DHT22 ----


float kp[2][N] = {
  {1808 / 3.5043, 0, 0}, // ch1  potencia "real" / numero de potencia de 4 decimales
  {0, 0, 0} // ch2
};
//------------------------------------

/*
  TOPE de caracteres!!!

  *state?
  *enable?
  *disable?
  *disOn 1473770640? fecha epoch futura local.
  *sample? toma T y H instantanea y pot. promedio del tiempo anterior.
  *power? Potencia promedio del tiempo actual
  *sendCode XXXX?

*/
#include <EEPROM.h>
#include <avr/interrupt.h>
#include <avr/io.h>
// #define TIMER_RESET  TCNT1 = 0
#define SAMPLE_SIZE  300

#include "ADE7753.h"
#include <SPI.h>

#define FIRM 1.9
#define DELAYCHIP 3 // delay entre chips

char decoded[SAMPLE_SIZE];

unsigned long TRESETP = 60;// 7200;// reset períodio del arduiono cada 2 horas.
unsigned long TOUTRESET = 300; // reset ESP cada 5 minutos si es que no recive ningun comando valido.

int change_count;
long time;
int incomingByte = 0;
byte estado;

unsigned int TimerValue[SAMPLE_SIZE];
char code[SAMPLE_SIZE];
char code0;

byte ADEpins[2][N] = {
  {7, 0, 0}, // ch1 Negro  , Problema wifi con el pin 7 del arduino
  {0, 0, 0}  // ch2 Blanco
};

byte ADEpinsR[2][N] = {
  {6, 0, 0}, // ch1 Negro
  {0, 0, 0}  // ch2 Blanco
};


//byte N=sizeof(Pins1); // numero de equipos conectados, Wenu Edificios
unsigned long aux1[2][N], t[2][N], t_ant[2][N], t_reset;
float aux, bux, bux1[2][N];
float v[2][N], I[2][N], Ipeak[2][N], p[2][N], P[2][N];

float kpos[2][N] = {
  {0, 0, 0}, // ch1
  {0, 0, 0} // ch2
};

// Paso 1
float kv[2][N] = {
  {877772.9 / 235.2, 0, 0}, // ch1  Promedio de las cuantas ADC / Voltaje del tester
  {0, 0, 0} // ch2
};

//Paso 2
float kios[2][N] = {  // Ofset de corriente en cuentas ADC
  {3204.6, 0, 0}, // ch1
  {0, 0, 0} // ch2
};

// Paso 3
float ki[2][N] = { //calulcadodo
  {364172.7 / 8.73, 0, 0}, // ch1
  {0, 0, 0} // ch2
};

float calt[2][N] = {
  {20.3, 0, 0}, // ch1
  {37, 0, 0}  // ch2
};

int hist[2][N] = {
  {0, 0, 0}, // ch1
  {0, 0, 0} // ch2
};

long  e[2][N], e_ant[2][N];
long e1, e2, e3;
unsigned long tpot = millis();

// byte inic = 0;

static inline int8_t sign(int val) {
  if (val < 0) return -1;
  if (val == 0) return 0;
  return 1;
}
void(* resetFunc) (void) = 0; //declare reset function @ address 0
void unzipSendTwoPatterns(); //function declaration
void unzipSend(); //function declaration
void remakeSend();
void tempDHT();

void deviceSetup(){
  switch(device){
  case FUJITSU:
    OFF = "ID4AC3C7C1C7C1C999C942C9C94C3CACACACACACA";    //Fujitsu
    periodo = 97; //Fujitsu
    decodedchar = 65;
    break;
  case KENDAL:
    OFF = "QH1C1C9C1C1C1C1C993C1C1C999C3C99999999997C3C92C1C1C1C5C3";  //Kendal
    periodo = 133; //Kendal
    decodedchar = 65;
    break;
  case DAITSU:
    OFF = "LuhkbkcmckfkakbkawjckjgkzLuhkbkcmckglbkawlakakdobkakdlbn";
    //harcoded times
    decodedchar = 97; //a
    decodedchar2 = 98; //b
    break;
  default:
    break;
}
  }

void setup() {
  deviceSetup();
  Serial.begin(115200);  // UART init
  if (debug == true) {
    Serial.print("Firmware: ");
    Serial.println(FIRM, DEC);
  }
  t_reset = millis();
  digitalWrite(8, LOW);
  //------ hablador
  estado = EEPROM.read(2);
  if(estado == 255){
    estado = 1;
    EEPROM.write(2, 1);

  }
  pinMode(9, OUTPUT);
  if (estado == 1) {
    digitalWrite(9, LOW); //enable
  } else {
    if(ALLOWDISABLE){digitalWrite(9, HIGH);} //disable
  }

  TCCR1A = 0x00;
  TCCR1B = 0x03;
  TIMSK1 = 0x00;
}

void loop() {

  byte inic = 0;

  delay(2100);
  DHT.read(10);
  tempDHT();

  ADE7753 meter = ADE7753();
  for (int j = 0; j < N; j++) { //se apaga el chip ADE7753 - PCB V3
    for (int i = 0; i < 2; i++) {
      pinMode(ADEpinsR[i][j], OUTPUT);
      digitalWrite(ADEpinsR[i][j], LOW);
    }
  }
  //pinMode(4, OUTPUT); //Borrado Climo
  //digitalWrite(4, LOW); //se apaga el chip ADE7753 - PCB V2//Borrado Climo

  delay(200);
  for (int j = 0; j < N; j++) { //se enciende el chip ADE7753 - PCB V3
    for (int i = 0; i < 2; i++) {
      digitalWrite(ADEpinsR[i][j], HIGH);
    }
  }
  //digitalWrite(4, HIGH); //se apaga el chip ADE7753 - PCB V2//Borrado Climo

  delay(10);
  for (int j = 0; j < N; j++) {
    for (int i = 0; i < 2; i++) {
      if (ADEpins[i][j] > 0) {
        delay(DELAYCHIP);
        pinMode(ADEpins[i][j], OUTPUT);
        digitalWrite(ADEpins[i][j], LOW);
        //ADE7753 meter = ADE7753();
        meter.setMode(44); //12 por default, 8 abilita el CF, 44 tira un software reset. ver pagina 55 del datasheet
        delay(10);
        meter.analogSetup(GAIN_4, GAIN_1, 0, 0, 0, 0); //G corriente, G voltaje, Offset corrinte, Offset Voltaje, Rango maximo de corriente, integrador corriente
        meter.resetStatus();
        //meter.setZeroCrossingTimeout(2);
        //meter.setApos(0);
        meter.energySetup(0x7FF, 0x0, 0x0, 0x7FF, 0x0, 0x0d);
        //meter.frecuencySetup(0x7FF, 0x7FF);
        //meter.energyGain(0x7FF, 0x7FF); //osfset WGAIN
        meter.setCurrentOffset(0);
        if (debug == true) {
          Serial.print("Pin:");
          Serial.print(ADEpins[i][j]);
          //        Serial.print(" ZX:");
          //        Serial.println(meter.getZeroCrossingTimeout());
          Serial.print(" OffsetCorr:");
          Serial.print(meter.getCurrentOffset());
          Serial.print(" IPEAK=");
          Serial.print(meter.getIpeak() / 40098.03419);
          meter.getIpeakReset();
          Serial.print(" VPEAK=");
          Serial.print(meter.getVpeak());
          meter.getVpeakReset();
          Serial.print(" Temp:");
          Serial.print(meter.getTemp() / 1.5 + calt[i][j], 0);
          Serial.print(" getMODE:");
          Serial.println(meter.getMode());
          digitalWrite(ADEpins[i][j], HIGH);
          Serial.flush(); // ADDED
        }
      }
    }
  }

  while (true) {

    //    responde

    if (Serial.available() > 0 || inic <= 1) { //fuerzo la primera entrada con inic
      if (inic > 1) {
        incomingByte = Serial.read();
        while (Serial.available() > 0 && incomingByte != '*' && incomingByte != '%' && incomingByte != '$' && incomingByte != '#') { //vacia el buffer
          incomingByte = Serial.read();
        }
        //if (incomingByte == '*' || incomingByte == '%' || incomingByte == '$' || incomingByte == '#') { //vacia el buffer
        //  t_reset = millis();
        //}
      }

      if (incomingByte == '+') {//antes *, ahora +
        delay(2);
        int i = 0;
        int aux = 0;
        while (aux < 20 && i <= SAMPLE_SIZE && incomingByte != '?') {
          if (Serial.available()) {
            incomingByte = Serial.read();
            code[i] = incomingByte;
            i++;
            aux = 0;
          } else {
            delay(1);
            aux++; //este es el timeout
          }
        }
        if (incomingByte == '?') //termina en ?
        {
          //code[i + 1] = 0; //RGH elimina el ?
          code[i - 1] = 0;
          if (i > 2) {
            if (estado == 1) {
              Serial.println("Sen");
              Serial.flush(); // ADDED
              remakeSend();
              
              //delay(10);
            } else {
              Serial.println("Dis!");
              Serial.flush(); // ADDED
            }
          } else {
            if (code[0] == 'd') { //d de disable
              if (estado == 1) {
                EEPROM.write(2, 0); //disable
              }
              estado = 0;
              OFF.toCharArray(code, OFF.length() + 1);
              remakeSend();
              
              if(ALLOWDISABLE){digitalWrite(9, HIGH);} //disable
              Serial.println("Dis");
              Serial.flush(); // ADDED
            }

            if (code[0] == 'e') { //e de enable
              if (estado == 0) {
                EEPROM.write(2, 1); //enable
              }
              estado = 1;
              digitalWrite(9, LOW);
              Serial.println("En");
              Serial.flush(); // ADDED
            }

            if (code[0] == 'a') { //*a? available, llega cada 20 segundos. Se resetea a los 10 minutos de no recibir esto.
              t_reset = millis();
              Serial.println("av");
              Serial.flush(); // ADDED
            }
          }
        }
      }
      char test;
      test = incomingByte;

      //--------------------------- Switch----------------------------------
      if ((test == '#') /*|| ((dt%10000)==0)*/ || (inic <= 1)) {

        for (int j = 0; j < N; j++) {
          for (int i = 0; i < 2; i++) { //medición a la basura
            delay(DELAYCHIP);
            if (ADEpins[i][j] > 0) {
              digitalWrite(ADEpins[i][j], LOW);
              meter.getVRMS();
              digitalWrite(ADEpins[i][j], HIGH);
            }
          }
          for (int i = 0; i < 2; i++) {
            if (ADEpins[i][j] > 0) {
              delay(DELAYCHIP);
              digitalWrite(ADEpins[i][j], LOW);
              aux = meter.vrms() / kv[i][j];
              if ((debug == false) && (aux < 80 || aux > 290)) {
                if (test == '#') {
                  Serial.println("p0.0|"); //error, voltaje fuera de rango
                  Serial.flush(); // ADDED
                  delay(2);
                  resetFunc();
                }
              }
              t[i][j] = millis();
              digitalWrite(ADEpins[i][j], LOW);
              I[i][j] = max((meter.irms() - kios[i][j]) / ki[i][j], 0);
              meter.getActiveEnergy();
              e[i][j] = meter.getActiveEnergy();
              Ipeak[i][j] = max((meter.getIpeak() - kios[i][j]) / ki[i][j], 0);
              meter.getIpeakReset();//no se usa para climo
              digitalWrite(ADEpins[i][j], HIGH);
              v[i][j] = aux;
            } else {
              v[i][j] = -1;
              I[i][j] = -1;
              p[i][j] = 0;
            }
          }
        }

        if (inic < 1) {
          for (int j = 0; j < N; j++) {
            for (int i = 0; i < 2; i++) {
              e_ant[i][j] = e[i][j];
              t_ant[i][j] = t[i][j];
            }
          }
        }

        if (inic > 1) {
          for (int j = 0; j < N; j++) {
            if (ADEpins[0][j] > 0) {// tiene que existir al menos el medidor 1

              for (int i = 0; i < 2; i++) {
                P[i][j] = float((e[i][j] - e_ant[i][j])) / float((t[i][j] - t_ant[i][j]));
                p[i][j] = kp[i][j] * P[i][j] - kpos[i][j];

                if (abs(p[0][j]) <= 3 && debug == false) {
                  p[0][j] = 0;
                }

                if (abs(p[1][j]) <= 3 && debug == false) {
                  p[1][j] = 0;
                }

                if (p[i][j] > 0 && hist[i][j] < 3) {
                  hist[i][j]++;
                }
                if (p[i][j] < 0 && hist[i][j] > -3) {
                  hist[i][j]--;
                }
                if (p[i][j] == 0) {
                  hist[i][j] = 0;
                }

                if (debug == true) {
                  Serial.print("hist");
                  Serial.print(hist[i][j], DEC);
                  Serial.print("sign(hist) ");
                  Serial.println(sign(hist[i][j]), DEC);
                  Serial.flush(); // ADDED
                }

                e_ant[i][j] = e[i][j];
                t_ant[i][j] = t[i][j];
              }

              if ( p[0][j] < 6000  || debug == true) { //20A
                if (test == '#' && sign(p[0][j]) == sign(hist[0][j]) || debug == true) {
                  if (debug == true) {
                    Serial.print("v=");
                    Serial.print((v[0][j] + v[1][j] * 0) / 1, 1);
                    Serial.print(",i=");
                    Serial.print(I[0][j], 2); //22A limitaciòn del cable, 31.5A limitación de 2W de disipación de la resistencia, 22A Ipeak satura el ch1, 18.5 AFujitsu 24000btu. Resistencia de 0.002ohm
                    Serial.print(",iPk=");
                    Serial.print(Ipeak[0][j], 2);
                    Serial.print(",P");
                    Serial.print(P[0][j], 4);
                    Serial.print(",");
                    Serial.flush(); // ADDED
                  }
                  Serial.print("p");
                  Serial.print(abs(p[0][j]), 1);
                  Serial.print("|");
                  Serial.flush(); // ADDED
                } else {
                  if (test == '#') {
                    Serial.println("p0.0|");
                    Serial.flush(); // ADDED
                  }
                }
              }
              else {
                if (test == '#') {
                  Serial.println("p0.0|");
                  Serial.flush(); // ADDED
                }
                delay(10);
                resetFunc();
              }
            }
          }

          Serial.println("");
          Serial.flush(); // ADDED
        }
        else {
          if (test == '#') {
            Serial.println("p0.0|");
            Serial.flush(); // ADDED
          }
        }
      }


      if (test == '%') {
        for (int j = 0; j < N; j++) {
          if (ADEpins[0][j] > 0) {
            Serial.println("---------------------------");
            Serial.print("id=");
            Serial.println(j + 1, DEC);
            Serial.flush(); // ADDED
            for (int i = 0; i < 2; i++) {
              if (ADEpins[i][j] > 0) {
                Serial.print("v");
                Serial.print(i + 1, DEC);
                Serial.print("[V]\t");
                Serial.flush(); // ADDED
              }
            }
            Serial.println("");
            Serial.flush(); // ADDED
          }

          for (int i = 0; i < 2; i++) {
            aux1[i][j] = 0;
          }

          for (int i = 0; i < 2; i++) { //medición a la basura
            if (ADEpins[i][j] > 0) {
              delay(DELAYCHIP);
              digitalWrite(ADEpins[i][j], LOW);
              meter.vrms();
              digitalWrite(ADEpins[i][j], HIGH);
            }
          }
          for (int r = 0; r < 20; r++) {
            for (int i = 0; i < 2; i++) {
              if (ADEpins[i][j] > 0) {
                delay(DELAYCHIP);
                digitalWrite(ADEpins[i][j], LOW);
                aux = meter.vrms();
                digitalWrite(ADEpins[i][j], HIGH);
                aux1[i][j] = aux1[i][j] + aux;
                Serial.print(aux / kv[i][j], 1);
                Serial.print("\t");
                Serial.flush(); // ADDED
              }
            }
            if (ADEpins[0][j] > 0) {
              Serial.println("");
              Serial.flush(); // ADDED
            }
          }
          if (ADEpins[0][j] > 0) {
            Serial.println("Promedios U. Fisicas");
            Serial.flush(); // ADDED
          }
          for (int i = 0; i < 2; i++) {
            if (ADEpins[i][j] > 0) {
              Serial.print(aux1[i][j] / (20.0 * kv[i][j]), 2);
              Serial.print("\t");
              Serial.flush(); // ADDED
            }
          }
          if (ADEpins[0][j] > 0) {
            Serial.println("");
            Serial.println("Promedios ADC");
            Serial.flush(); // ADDED
          }
          for (int i = 0; i < 2; i++) {
            if (ADEpins[i][j] > 0) {
              Serial.print(aux1[i][j] / 20.0, 1);
              Serial.print("\t");
              Serial.flush(); // ADDED
            }
          }
          if (ADEpins[0][j] > 0) {
            Serial.println("");
            Serial.flush(); // ADDED
          }
        }
      }

//      if (test == '$') {
        if (test == '&') {
        for (int j = 0; j < N; j++) {
          if (ADEpins[0][j] > 0) {
            delay(DELAYCHIP);
            Serial.println("---------------------------");
            Serial.print("id=");
            Serial.println(j + 1, DEC);
            Serial.flush(); // ADDED
            for (int i = 0; i < 2; i++) {
              if (ADEpins[i][j] > 0) {
                Serial.print("i");
                Serial.print(i + 1, DEC);
                Serial.print("[A]\t");
                Serial.print("ios");
                Serial.print(i + 1, DEC);
                Serial.print("[A]\t");
                Serial.flush(); // ADDED
              }
            }
            Serial.println("");
            Serial.flush(); // ADDED
          }

          for (int i = 0; i < 2; i++) {
            bux1[i][j] = 0;
          }

          for (int i = 0; i < 2; i++) { //medición a la basura
            if (ADEpins[i][j] > 0) {
              delay(DELAYCHIP);
              digitalWrite(ADEpins[i][j], LOW);
              meter.irms();
              digitalWrite(ADEpins[i][j], HIGH);
            }
          }
          for (int r = 0; r < 20; r++) {
            for (int i = 0; i < 2; i++) {
              if (ADEpins[i][j] > 0) {
                delay(DELAYCHIP);
                digitalWrite(ADEpins[i][j], LOW);
                //delay(10);
                bux = meter.irms();
                digitalWrite(ADEpins[i][j], HIGH);
                bux1[i][j] = bux1[i][j] + bux / 20;
                Serial.print(bux / ki[i][j], 2);
                Serial.print("\t");
                Serial.print((bux - kios[i][j]) / ki[i][j], 2);
                Serial.print("\t");
                Serial.flush(); // ADDED
              }
            }
            if (ADEpins[0][j] > 0) {
              Serial.println("");
              Serial.flush(); // ADDED
            }
          }
          if (ADEpins[0][j] > 0) {
            Serial.println("Promedios U. Fisicas");
            Serial.flush(); // ADDED
          }
          for (int i = 0; i < 2; i++) {
            if (ADEpins[i][j] > 0) {
              Serial.print(bux1[i][j] / ki[i][j], 3);
              Serial.print("\t");
              Serial.print((bux1[i][j] - kios[i][j]) / ki[i][j], 3);
              Serial.print("\t");
              Serial.flush(); // ADDED
            }
          }
          if (ADEpins[0][j] > 0) {
            Serial.println("");
            Serial.println("Promedios ADC");
            Serial.flush(); // ADDED
          }
          for (int i = 0; i < 2; i++) {
            if (ADEpins[i][j] > 0) {
              Serial.print(bux1[i][j], 1);
              Serial.print("\t");
              Serial.print((bux1[i][j] - kios[i][j]), 1);
              Serial.print("\t");
              Serial.flush(); // ADDED
            }
          }
          if (ADEpins[0][j] > 0) {
            Serial.println("");
            Serial.flush(); // ADDED
          }
        }
      }
    }

    if (inic <= 10) {
      inic++;
    }

    if (millis() - t_reset > TOUTRESET * 1000) {
      if (millis() > t_reset) {
        pinMode(8, OUTPUT);
        delay(500);
        Serial.println("ResetESP");
        Serial.flush(); // ADDED
        delay(500);
        pinMode(8, INPUT);
      }
      t_reset = millis();
    }

    if (millis() > TRESETP * 1000 ) {
      Serial.println("ResetARD");
      Serial.flush(); // ADDED
      delay(2);
      resetFunc();
    }

    dt = tf-to;
    tf = millis();

    if ( (incomingByte == '#') ){
      tempDHT();
    }
  }//while true
}

void tempDHT(){

  float temp = DHT.temperature;
  float humi = DHT.humidity;

  if(DHT.temperature==-999) temp = 26;
  if(DHT.humidity==-999) humi = 26;

  Serial.print("");
  Serial.print(temp,1);
  Serial.print("/");
  Serial.print(humi,1);
  Serial.println("|");
  Serial.flush();
}

unsigned int remake_0 (char turn) {

    unsigned int codeR[]={0};

    if (turn == '4') {
      codeR[0]   = 650;
    }
    else if (turn == '3') {
      codeR[0]   = 9030;
    }
    else if (turn == '2') {
      codeR[0]   =  650;
    }
    else if (turn == '1') {
      codeR[0]   =  650;
    }
    else if (turn == '0') {
      codeR[0]   = 650;
    }

    return codeR[0];
}

unsigned int remake_1 (char turn) {

    unsigned int codeR[]={0};

    if (turn == '4') {
      codeR[0]   =   0;
    }
    else if (turn == '3') {
      codeR[0]   = 4500;
    }
    else if (turn == '2') {
      codeR[0]   = 20000;
    }
    else if (turn == '1') {
      codeR[0]   = 1664;
    }
    else if (turn == '0') {
      codeR[0]   = 560;
    }

    return codeR[0];

}

void remakeSend () {

  unsigned int command[139];
  int khz = 38;

  for (int i = 0 ; i < sizeof(code) ; i++ ) {

    if (i == (sizeof(code)-1)){

      command[(2*i)] = remake_0(code[i]);

    }
    else{

      command[(2*i)]   = remake_0(code[i]);
      command[(2*i)+1] = remake_1(code[i]);

    }

  }

  irsend.sendRaw(command, sizeof(command)/sizeof(int), khz);

}
